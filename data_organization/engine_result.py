# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 12:36:07 2020

@author: Siddharth
"""

import chess
import chess.engine
import re


def engine_eval(game, engine, user_color):
    
    """
    Returns the evaluation of the players moves and corresponding best engine moves
    
    Parameters
    ----------
    df: dataframe
        Dataframe containing all game data for a user in a month
    
    game: chess.game object
        The game file to be analyzed
        
    ENGINE_PATH: str
        Location of the stockfish file on the users local drive
        
    Returns
    -------
    player_move_eval: list
        List of all player move evaluations. List of float and str
        
    engine_move_eval: list
        List of corresponding engine move evaluations. List of float and str
    
    """
    
    #ENGINE_PATH = r'C:\Users\Siddharth\chess\data_organization\stockfish_20090216_x64.exe')
    eva = engine_analysis(game, engine, user_color)
    player_move_eval = clean_evaluation(eva)
    #engine_move_eval = clean_evaluation(best_eva)
    return player_move_eval #, engine_move_eval


    
def clean_evaluation(evaluate):

    """
    Returns the evaluation list after converting numbers to float and removing extra text
    
    Parameters
    ----------
    evaluate: list
        List of move evaluations. List of float and str
        
    Returns
    -------
    evaluation: list
        List of evaluations after removing extra text
    
    """
    
    evaluation = []
    for index, ele in enumerate(evaluate):
        
        
        if ele.find('Cp(') != -1:
            
            start = ele.find('Cp(') + 3
            end = ele.find('),', start)
            score = int(ele[start:end])/100
            evaluation.append(score)


        else:
            #start = ele.find('Mate(') + 5
            #end = ele.find('),', start) 
            #score = 'Mate' + ele[start:end]
            
            if ele.find('+') != -1:
                score = 10000
            else:
                score = -10000
            
            evaluation.append(score)
        
    
    return evaluation 



def engine_analysis(game, engine, user_color):
    
    """
    Returns the raw evaluation of the players moves and corresponding best engine moves
    
    Parameters
    ----------
    game: chess.game object
        All the game data is stored as a game object for analysis
        
     ENGINE_PATH: str
        Location of the stockfish file on the users local drive   
        
    Returns
    -------
    evaluation: list
        List of all player move evaluations. List of float and str
        
    best_case_evaluation: list
        List of corresponding engine move evaluations. List of float and str
    
    """
    
    
    board = game.board() 
    evaluation = []
    
    for number, move in enumerate(game.mainline_moves()):     
        #if (number%2 == user_color):
         #   board.push(move)
         #   continue
        board.push(move) 
        info = engine.analyse(board, chess.engine.Limit(time=0.1))                                                    
        evaluation.append(str(info['score']))
    
    """
    board = game.board()
    board_engine = game.board()
    best_case_evaluation = []
    
    for move in game.mainline_moves():

        result = engine.play(board, chess.engine.Limit(time=0.2))
        board.push(move)
        board_engine.push(result.move)
        info = engine.analyse(board_engine, chess.engine.Limit(time=0.2)) 
        best_case_evaluation.append(str(info['score']))
        board_engine.pop()
        board_engine.push(move)
    
    #board = chess.Board(node)
    
    #print(info)
    """
    return evaluation#, best_case_evaluation