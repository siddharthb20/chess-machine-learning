# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 06:04:02 2020

@author: Siddharth
"""
import datetime


def time_stamps(df, game_number):
    
    
    """
    Returns a list of move time stamps for black and white for a specific game
    
    Parameters
    ----------
    df : dataframe with game data of the user
        Data from chess.com arranged into the dataframe for further usage
        
    game_number : int
        Position of the game to be analyzed in the list
        
    Returns
    -------
    white_time, black_time : lists of int type values
         Lists containing time stamps of all the moves from black and white for the given game number   
    
    """
    
    #print("Printing from move_timestamps.py")
    #print(pgn)
    
    max_time = int(df["base_time"][game_number])
    game = df["pgn_moves_clk"][game_number]
    INCREMENT = int(df["increment"][game_number])
    
    white_time = [float(max_time)]
    black_time = [float(max_time)]
    times = []
    time_var = ''
    
    i = 0
    
    while i < len(game):
        
        # print(i, len(pgn[0]))
        time_var = ''
        if game[i] == '{':
        
            j = i + 7
            
            if game[i + 14] == '.':
                k = i + 16
            
            else:
                k = i + 14
            
            while j < k:
                time_var += game[j]
                
                if k == i + 14 and j == k-1:
                    time_var += '.0'
                j += 1
            
            i = j
            
            times.append(time_var)
        
        else:
            
            i += 1
        
    
    i = 0
    while i < len(times):
        
        time_object = datetime.datetime.strptime(times[i], '%H:%M:%S.%f').time()
        time_obj = time_object.hour*3600 + time_object.minute*60 + time_object.second
        time_obj = float(time_obj) + float(time_object.microsecond/1000000)
        
        if i%2 == 0:
            white_time.append(time_obj + INCREMENT)
            
        else:
            black_time.append(time_obj + INCREMENT)
            
        i += 1
            
    
    #print("\nWhite move times : ", white_time)
    #print("\nBlack move times : ", black_time)
    
    return white_time, black_time