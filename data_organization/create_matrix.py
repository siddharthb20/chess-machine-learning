# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 08:57:46 2020

@author: Siddharth
"""

import numpy as np
import matplotlib.pyplot as plt
from .move_timestamps import time_stamps
from .engine_result import engine_eval
#from .move_accuracy import move_accuracy
#from .data_move_time import move_time
    
                        
def data_organization (df, games, GAME_NUMBER, engine):
    
    """
    Creates 3D tensor for input to NN
    
    Parameters
    ----------
    df: dataframe
        Dataframe containing all game data for a user in a month
    
    games: list
        List of all game objects for every game
        
    GAME_NUMBER: int
        The serial number of the game to be analyzed
        
    ENGINE_PATH: str
        Location of the stockfish file on the users local drive
        
    Returns
    -------
    
    
    
#print("Printing from create_matrix.py")
#time_controls = list(df.TimeControl.unique())
#GAME_NUMBER = 4
TIME_CONTROL = df["base_time"][GAME_NUMBER]
INCREMENT = df["increment"][GAME_NUMBER]


print(TIME_CONTROL)

white_time, black_time = time_stamps(df, GAME_NUMBER)

game = games[GAME_NUMBER]
time_left = move_time(white_time, INCREMENT)
print(time_left, white_time)

#player_move_eval, engine_move_eval = engine_eval(game, engine)
#accuracy_matrix = move_accuracy(player_move_eval, engine_move_eval)
#print("Player move evaluations:", player_move_eval, "\nEngine evaluations", engine_move_eval)

"""
    X_data = []
    y_labels = []
    data = []
    print("\n\nGenerating_data")
    
    for i in range(len(games)):
        
        GAME_NUMBER = i
        game = games[i]
        print("Games assessed: ", i)
        
        if i == 0:
            continue 
        color = df["user_color"][GAME_NUMBER]
        result = df["user_result"][GAME_NUMBER] + 1
        
    
        white_ELO = int(df["BlackElo"][GAME_NUMBER])
        black_ELO = int(df["WhiteElo"][GAME_NUMBER])
                        
        
        white_times, black_times = time_stamps(df, GAME_NUMBER)
            
        #evaluation = engine_eval(game, engine, color)
        
        for i in range(len(black_times) - 1):
            data = [white_times[i], white_ELO, black_times[i], black_ELO]
            X_data.append(data)
            y_labels.append(result)
    
    X = np.array(X_data)
    y = np.array(y_labels)
    X = X.reshape((1, -1))
    np.savetxt('data,txt', X)
    X_da = np.loadtxt('data.txt')
    X_da = X_da.reshape(-1, 3)
    print(X.shape, X_da.shape)
    
    
    return X_data, y_labels