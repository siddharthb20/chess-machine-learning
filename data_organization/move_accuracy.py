# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 20:25:56 2020

@author: Siddharth
"""

def move_accuracy(player_move_eval, engine_move_eval):
    
    #print(player_move_eval)
    #print("Engine:", engine_move_eval)
    move = []
    for i in range(len(player_move_eval)):
        if isinstance(player_move_eval[i], float) == True and isinstance(engine_move_eval[i], float) == True:
            delta = -1 * engine_move_eval[i] - -1 * player_move_eval[i]
            move.append(nomenclature(delta))  
            
        elif isinstance(player_move_eval[i], float) == True and isinstance(engine_move_eval[i], float) == False:
            delta = False
            if engine_move_eval[i].find('+') != -1:
                move.append(nomenclature(delta, engine_mate = True))
            else:
                move.append('Forced')
    
        elif isinstance(player_move_eval[i], float) == False and isinstance(engine_move_eval[i], float) == True:
            delta = False
            if player_move_eval[i].find('-') != -1:
                move.append('Missed win')
            else:
                move.append(nomenclature(delta, engine_mate = False, player_mate = True))
            
        else:
            move.append('Best move')
            
    return move
            
def nomenclature(delta, engine_mate = False, player_mate = False):
    
    if delta >= 0:
        if delta == 0:
            return 'Best'
    
        elif delta < 0.1:
            return 'Excellent'
    
        elif delta < 0.5:
            return 'Good'
    
        elif delta < 1:
            return 'Inaccuracy'
    
        elif delta < 3:
            return 'Mistake'
    
        else:
            return 'Blunder'
        
    elif delta == False:
        if engine_mate == False and player_mate == True:
            return 'Brilliant'
        
        elif engine_mate == True and player_mate == False:
            return 'Missed win'
        
        elif engine_mate == True and player_mate == True:
            return 'Best'
        
    else:
        return 'Brilliant'