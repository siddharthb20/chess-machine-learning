# -*- coding: utf-8 -*-
"""
Created on Fri Dec 25 10:28:05 2020

@author: Siddharth
"""

import tensorflow as tf
from tensorflow import keras
from keras import backend
import numpy as np
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy

def data_prep(X, y):
    
    X_data = np.array(X)
    y_labels = np.array(y)
    print("\n\n", X_data.shape)
    
    #X_data, y_labels = shuffle(X_data, y_labels)
    
    scaler = MinMaxScaler(feature_range = (0, 1))
    #scaled_train_samples = scaler.fit_transform(X_data.reshape(-1, 1))
    
    #for i in scaled_train_samples:
     #   print(i)
    print (X_data)
    basic_model(X_data, y_labels)
        
def basic_model(X, y):
    
    BATCH_SIZE = 10
    EPOCHS = 150
    
    model = Sequential([
        Dense(units=16, input_shape=(4,), activation='relu'),
        Dense(units=64, activation='relu'),
        Dense(units=64, activation='relu'),
        Dense(units=3, activation='softmax')])
    
    model.summary()
    
    model.compile(optimizer=Adam(learning_rate = 0.0001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    
    model.fit(x=X, y=y, batch_size=BATCH_SIZE, epochs=EPOCHS, shuffle=True, verbose=2)