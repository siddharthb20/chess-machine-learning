#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 23:02:41 2020

@author: pratik
"""

from chesscompy.user import User
from chesscompy.pgn_processing import chesscom_pgn_parser
from chesscompy.pgn_processing import strip_click_time


#from visualizations.board_plot import show_board

#from game_stats.move_timestamps import time_stamps
#from game_stats.win_loss_calculator import win_loss

from game_stats.opening_analysis import opening

from data_organization.create_matrix import data_organization
from data_organization.keras_basics import data_prep


import chess.engine


import chess
import pandas
import matplotlib.pyplot as plt

        
"""              
Pratik - proKrasstinator
Siddharth  - sidb252
"""
username = "RaunakSadhwani2005"
user = User(username)
game_number = 6

ENGINE_PATH = r'C:\Users\Siddharth\chess\data_organization\stockfish_20090216_x64.exe'

engine = chess.engine.SimpleEngine.popen_uci(ENGINE_PATH)

# Archived games
games, headers, pgn_text = user.get_archived_games()

headers = User.get_headers(games, as_dataframe=True)

# Games from a month

# Import games of a user and pass it as an argument to the parser
games, headers, pgn_text = chesscom_pgn_parser(user.get_games_by_month(year="2020", month="12"))

df = user.enrich_dataframe(games, pgn_text)
"""
#%% Statistics

from chesscompy.chesscom_stats import calc_timecontrol_stats, calc_bw_record, calc_ECO_stats

api_stats = user.user_stats_api()
stats = calc_bw_record(df)

from visualizations.stat_views import plot_pie

plot_pie(list(stats["white"]["record"].values()), labels=list(stats["white"]["record"].keys()))
plot_pie(list(stats["black"]["record"].values()), labels=list(stats["black"]["record"].keys()))
"""
#%%
pgn_string_with_clk_time = pgn_text[game_number]

#white_time_left, black_time_left = time_stamps(df, game_number)

pgn_string = strip_click_time(pgn_string_with_clk_time)
#win_loss(df)
#opening(df, games)

#print("\n\n", pgn_string)

X, y = data_organization(df, games, game_number, engine)

data_prep(X, y)


"""
# Import header data from all games
headers = User.get_headers(games, as_dataframe=True)
headers.sort_values(by=["UTCDate", "UTCTime"], inplace=True, ignore_index=True)


#engine = chess.engine.SimpleEngine.popen_uci('/usr/local/Cellar/stockfish/12/bin/stockfish')

board = chess.Board(headers.loc[404, "CurrentPosition"])

#info = engine.analyse(board, chess.engine.Limit(depth=20))

board_position = chess.svg.board(board, size=350)

# Plotting the board position
show_board(board_position)

#info["score"][""]
#print("\n\n", "Result: {}".format(headers.loc[404, "Termination"]))
"""
