"""
Chess.com Published Data API URLs


@author: Pratik
"""

class Paths():
    
    """
    URL strings to various resources in Chess.com api
    
    Attributes
    ----------
    base: str
        URL base string of the Chess.com published data api
        
    player: str
        URL string of the player resource
    
    Methods
    -------
    __init__()
        Instantiates an object of Paths
    """
    
    def __init__(self):
        
        """
        Instantiates an object of Paths
        """
        
        self.base = "https://api.chess.com/pub/"
        self.player = "https://api.chess.com/pub/player/"
        
                
                
        

