"""
Chess.com User statistics from dataframe 


@author: Pratik
"""



def calc_record(df):
    
    """
    Returns the win/loss statistics of the user from dataframe
    
    Parameters
    ----------
    df: pandas.dataframe
        datafrmae of user games
        
    Returns
    -------
    stats: dict
        A dictionary of the win/loss statistics of the user from dataframe
    """
    
    outcomes = df.user_result.value_counts()
    
    mapping = {"win": 1,
               "loss": -1,
               "draw": 0}
    
    record = {}
    
    for result in mapping.keys():
        
        try:
            record[result] = outcomes.loc[mapping[result]]
        except KeyError:
            record[result] = 0

    
    stats =  {"record": record}
    
    return stats

def calc_bw_record(df):
        
    """
    Returns the win/loss statistics of the user with black and white pieces from dataframe
    
    Parameters
    ----------
    df: pandas.dataframe
        datafrmae of user games
        
    Returns
    -------
    stats: dict
        A dictionary of the win/loss statistics of the user from dataframe
    """
    
    mapping = {"white": 1,
              "black": 0}
    
    stats = {}
    
    for color in mapping.keys():
        
        try:
            stats[color] = calc_record(df[df["user_color"]==mapping[color]])
        except KeyError:
            stats[color] = {"win": 0,
                               "loss": 0,
                               "draw": 0}

    
    return stats

def calc_timecontrol_stats(df):
    
    """
    Returns the win/loss statistics of the user from dataframe
    
    Parameters
    ----------
    df: pandas.dataframe
        datafrmae of user games
        
    Returns
    -------
    stats: dict
        A dictionary of the win/loss statistics of the user from dataframe
    """
    stats = {}
    
    mapping = {"Blitz": "chess_blitz",
               "Bullet": "chess_bullet",
               "Rapid": "chess_rapid"}
    
    
    for tc in mapping.keys():
        
        try:
            stats[mapping[tc]] = calc_bw_record(df[df["time_control"]==tc])
        except KeyError:
            continue
    
    return stats


    


def calc_ECO_stats(df):
    
    """
    Returns the win/loss statistics of the user from each ECO opening in the dataframe
    
    Parameters
    ----------
    df: pandas.dataframe
        datafrmae of user games
        
    Returns
    -------
    stats: dict
        A dictionary of the win/loss statistics of the user for each Opening 
        code as black and white
    """

    eco_codes = df.ECO.value_counts()

    stats = {}
    
    for index, count in eco_codes.iteritems():
        
        
        try:
            stats[index] = calc_timecontrol_stats(df[df["ECO"]==index])
        except KeyError:
            continue

    
    return stats
        
        
    
    