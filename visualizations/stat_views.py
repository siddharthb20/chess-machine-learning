"""
User statistics visualizations


@author: Pratik
"""

import matplotlib.pyplot as plt
import seaborn as sns


def plot_pie(data, labels, options={"colors": ["#658b38", "#8c000f", "#95a3a6"],
                                    "autopct": "%1.1f%%",
                                    "shadow": True,
                                    "startangle": 90}):
    
    """
    plots a pie chart
    
    Parameters
    ----------
    data: list
        sizes of each piece of pie
    labels: list
        labels for each piece of pie
    options: dict
        parameters passed to matplotlib.pyplot.pie function
        
    Returns
    -------

    """
    
    fig1, ax1 = plt.subplots()
    ax1.pie(data, 
            labels=labels,
            colors=options["colors"],
            autopct=options["autopct"],
            shadow=options["shadow"],
            startangle=options["startangle"]
            )
    
    ax1.axis("equal")
    
    plt.show()
    
    